﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Tetris
{
    public partial class Field : UserControl
    {
        /* 動かすテトリミノ */
        public static int[,] move_tetrmino = new int[4, 4];

        ///* 着地地点のテトリミノ */
        //public static int[,] landing_tetrmino = new int[4, 4];


        /* ホールド中のテトリミノ */
        public static int[,] hold_tetrmino = new int[4, 4];

        /* 回転させるための仮のテトリミノ */
        public static int[,] tentative_tetrmino = new int[4, 4];
        
        /* 次のテトリミノ */
        public static int[,] next_tetrmino = new int[4, 4];
        /* フィールド定義 */
        public static int[,] field_tery = new int[22, 10];
        /* テトリミノの地形 */
        public static int[,] terrain_tetmino = new int[22, 10];

        public static int score = 0;
        public static int high_score = 0;

        public static int g = 1;

        int time_cnt = 0;
        int terrain_time_cnt = 0;

        int g_cnt = 0;

        int under_mino = 0;
        int under_wall = 0;

        int rotate_cnt = 1;
        int hold_cnt = 0;

        /* 落ちる速さ */
        int time_cnt_value = 30;
        /* 着地の長さ */
        int terrai_time_cnt_value = 20;

        /* フィールドの大きさ */
        public int col = 10;
        public int row = 22;

        /* 操作するテトリミノを表す添え字 */
        int y = -2;      //高さ
        int y2 = -2;         //テトリミノの着地地点の高さ
        int x = 3;      //横
 
        //難易度
        public int difficulty = 2;

        /* 色 */
        Color color1 = Color.FromArgb(205, 221, 135);          //白
        Color color2 = Color.FromArgb(147, 168, 49);            //薄い黒
        Color color3 = Color.FromArgb(115, 128, 5);            //濃い黒
        Color color4 = Color.FromArgb(43, 58, 1);          //黒
        

        /* 動かすテトリミノのリスト */
        private List<Point> tetr_ls = new List<Point>();

        /* 着地テトリミノのリスト */
        private List<Point> landing_tetr_ls = new List<Point>();

        /* 地形のリスト */
        private List<Point> terrain_ls = new List<Point>();

        /* 地形のリスト */
        private List<Point> Tisappear_terrain_ls = new List<Point>();

        /* ステータス */
        private enum tetris_state
        {
            waithing,
            moving,
            stop
        };

        /* キーイベント用 */
        private Tetris.Field.tetris_state state = tetris_state.waithing;

        private Dictionary<Keys, Action> key_waiting = new Dictionary<Keys, Action>();
        private Dictionary<Keys, Action> key_moving = new Dictionary<Keys, Action>();
        private Dictionary<Keys, Action> key_stop = new Dictionary<Keys, Action>();

        private Dictionary<Tetris.Field.tetris_state, Dictionary<Keys, Action>> 
            key_maps = new Dictionary<Tetris.Field.tetris_state, Dictionary<Keys, Action>>();


        public Field()
        {
            InitializeComponent();
            Evnt();
            this.DoubleBuffered = true; //ダブルバッファ・ちらつき防止

        }

        private void Field_Load(object sender, EventArgs e)
        {

            start_lbl.Visible = true;
            start_lbl.Left = (this.Width / 2) - (start_lbl.Width / 2);

            this.DoubleBuffered = true; //ダブルバッファ・ちらつき防止

            gameover_lbl.Left = (this.Width / 2) - (gameover_lbl.Width / 2);
            gameover_lbl.Visible = false;
            gameover_lbl2.Left = (this.Width / 2) - (gameover_lbl.Width / 2);
            gameover_lbl2.Visible = false;



            easy.Left = (this.Width / 2) - (easy.Width / 2);
            easy.Visible = true;
            nomal.Left = (this.Width / 2) - (nomal.Width / 2);
            nomal.Visible = true;
            hard.Left = (this.Width / 2) - (hard.Width / 2);
            hard.Visible = true;

            nomal.BackColor = color4;
            nomal.ForeColor = color1;


            this.Paint += new PaintEventHandler(Fm_Paint);

            this.Focus();

        }

        // イベントの初期設定
        private void Evnt()
        {

            Action Escape_evnt = () =>
            {
                End?.Invoke(this, EventArgs.Empty);
            };

            Action Enter_evnt = () =>
            {
                tetr_ls.Clear();
                terrain_ls.Clear();

                Field_clear();
                Terrain_clear();

                y = -2;
                x = 3;

                Next_tetymino();
                
//                System.Threading.Thread.Sleep(30);
                New_tetrmino();
                Next_draw?.Invoke(this, EventArgs.Empty);

                Move_tetrmino();
                Hold_clear();

                g = 1;
                Level_draw?.Invoke(this, EventArgs.Empty);
                
                score = 0;
                time_cnt = 0;

                Level_draw?.Invoke(this, EventArgs.Empty);
                Score_draw?.Invoke(this, EventArgs.Empty);


                terrain_time_cnt = 0;
                under_mino = 0;
                under_wall = 0;

                Hold_clear();
                Hold_draw?.Invoke(this, EventArgs.Empty);


                easy.Visible = false;
                nomal.Visible = false;
                hard.Visible = false;

                start_lbl.Visible = false;
                timer1.Enabled = true; //タイマースタート

                state = tetris_state.moving;
            };

            Action F1_evnt = () =>
            {

                tetr_ls.Clear();
                terrain_ls.Clear();

                Field_clear();
                Terrain_clear();

                y = -2;
                x = 3;

                score = 0;
                time_cnt = 0;

                Level_draw?.Invoke(this, EventArgs.Empty);
                Score_draw?.Invoke(this, EventArgs.Empty);


                Next_tetymino();
                System.Threading.Thread.Sleep(20);

                New_tetrmino();

                Move_tetrmino();

                g = 1;
                Level_draw?.Invoke(this, EventArgs.Empty);

                terrain_time_cnt = 0;
                under_mino = 0;
                under_wall = 0;

                Hold_clear();
                Hold_draw?.Invoke(this, EventArgs.Empty);

                gameover_lbl.Visible = false;
                gameover_lbl2.Visible = false;

                timer1.Enabled = true;

                state = tetris_state.moving;
            };

            Action Down_evnt = () =>
            {
                int down = 0;

                int next_dowm = 0;

                /* ぶつかっていない箇所を探す */
                for (int i = 0; i < row; i++)
                {
                    for (int j = 0; j < col; j++)
                    {
                        if (field_tery[i, j] == 1)
                        {
                            if (i == (row - 1))
                            {
                                //下ダメ
                                down = 1;
                            }
                        }
                    }
                }

                for (int i = 0; i < (row - 1); i++)
                {
                    for (int j = 0; j < col; j++)
                    {
                        if ((field_tery[i, j] == 1) && (terrain_tetmino[(i + 1), j] == 1))
                        {
                            next_dowm = 1;
                        }
                    }
                }
                if ((next_dowm == 0) && (down == 0))
                {
                    y += 1;
                    score += 5;
                    Score_draw?.Invoke(this, EventArgs.Empty);

                    Move_tetrmino();
                }
            };

            Action R_evnt = () =>
            {
                int r = 0;

                int next_r = 0;

                /* ぶつかっていない箇所を探す */
                for (int i = 0; i < row; i++)
                {
                    for (int j = 0; j < col; j++)
                    {
                        if (field_tery[i, j] == 1)
                        {
                            if (j == (col - 1))
                            {
                                //右ダメ
                                r = 1;
                            }

                        }
                    }
                }

                for (int i = 0; i < row; i++)
                {
                    for (int j = 0; j < (col - 1); j++)
                    {
                        if ((field_tery[i, j] == 1) && (terrain_tetmino[i, (j + 1)] == 1))
                        {
                            next_r = 1;
                        }
                    }
                }
                if ((next_r == 0) && (r == 0))
                {
                    x += 1;
                    Move_tetrmino();
                }
            };

            Action L_evnt = () =>
            {
                int l = 0;

                int next_l = 0;

                /* ぶつかっていない箇所を探す */
                for (int i = 0; i < row; i++)
                {
                    for (int j = 0; j < col; j++)
                    {
                        if (field_tery[i, j] == 1)
                        {

                            if (j == 0)
                            {
                                //左ダメ
                                l = 1;
                            }
                        }
                    }
                }

                for (int i = 0; i < row; i++)
                {
                    for (int j = 1; j < col; j++)
                    {
                        if ((field_tery[i, j] == 1) && (terrain_tetmino[i, (j - 1)] == 1))
                        {
                            next_l = 1;
                        }
                    }
                }

                if ((next_l == 0) && (l == 0))
                {
                    x -= 1;
                    Move_tetrmino();
                }
            };

            Action Speas_evnt = () =>
            {
                Rotate_tetymino();
                Move_tetrmino();

            };

            Action Z_evnt = () =>
            {
                Auto_tetrmino();
                Move_tetrmino();

            };

            Action A_evnt = () =>
            {
                chang_hold();
            };

            Action Up_difficulty = () =>
            {
                if (1 < difficulty)
                {
                    difficulty--;
                }

                if (difficulty == 1)
                {
                    easy.BackColor = color4;
                    nomal.BackColor = System.Drawing.Color.Transparent;
                    hard.BackColor = System.Drawing.Color.Transparent;

                    easy.ForeColor = color1;
                    nomal.ForeColor = color4;
                    hard.ForeColor = color4;

                    time_cnt_value = 10;

                }
                else if (difficulty == 2)
                {
                    easy.BackColor = System.Drawing.Color.Transparent;
                    nomal.BackColor = color4;
                    hard.BackColor = System.Drawing.Color.Transparent;

                    easy.ForeColor = color4;
                    nomal.ForeColor = color1;
                    hard.ForeColor = color4;

                    time_cnt_value = 20;

                }


                this.Focus();

            };

            Action Down_difficulty = () =>
            {
                if (difficulty < 3)
                {
                    difficulty++;
                }

                
                if (difficulty == 2)
                {
                    easy.BackColor = System.Drawing.Color.Transparent;
                    nomal.BackColor = color4;
                    hard.BackColor = System.Drawing.Color.Transparent;

                    easy.ForeColor = color4;
                    nomal.ForeColor = color1;
                    hard.ForeColor = color4;

                    time_cnt_value = 20;


                }
                else if (difficulty == 3)
                {
                    easy.BackColor = System.Drawing.Color.Transparent;
                    nomal.BackColor = System.Drawing.Color.Transparent;
                    hard.BackColor = color4;

                    easy.ForeColor = color4;
                    nomal.ForeColor = color4;
                    hard.ForeColor = color1;

                    time_cnt_value = 30;

                }

                this.Focus();


            };

            Action F1_evnt_Start = () =>
            {
                state = tetris_state.waithing;

                start_lbl.Visible = true;

                gameover_lbl.Visible = false;
                gameover_lbl2.Visible = false;

                easy.Visible = true;
                nomal.Visible = true;
                hard.Visible = true;

                timer1.Enabled = false;

                this.Invalidate();
            };


            /* スタート画面 */
            key_waiting.Add(Keys.Escape, Escape_evnt);
            key_waiting.Add(Keys.Enter, Enter_evnt);
            key_waiting.Add(Keys.Up, Up_difficulty);
            key_waiting.Add(Keys.Down, Down_difficulty);


            /* 動いている最中 */
            key_moving.Add(Keys.Escape, Escape_evnt);
            key_moving.Add(Keys.F1, F1_evnt);
            key_moving.Add(Keys.Down, Down_evnt);
            key_moving.Add(Keys.Right, R_evnt);
            key_moving.Add(Keys.Left, L_evnt);
            key_moving.Add(Keys.Space, Speas_evnt);
            key_moving.Add(Keys.Z, Z_evnt);
            key_moving.Add(Keys.A, A_evnt);

            /* ゲームオーバー */
            key_stop.Add(Keys.Escape, Escape_evnt);
            key_stop.Add(Keys.F1, F1_evnt_Start);


            key_maps.Add(tetris_state.waithing, key_waiting);
            key_maps.Add(tetris_state.moving, key_moving);
            key_maps.Add(tetris_state.stop, key_stop);
        }

        /* キーが押された時 */
        private void Field_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            var currentKeyMap = key_maps[state];
            if (currentKeyMap.ContainsKey(e.KeyCode))
            {
                var action = currentKeyMap[e.KeyCode];
                action();
            }

            this.Focus();
//            this.ActiveControl = this;
        }

        /* テトリミノの位置を移動 */
        public void Move_tetrmino()
        {
            /* 今までの描画とフィールドをクリア */
            tetr_ls.Clear();
            Field_clear();
            landing_tetr_ls.Clear();

            Landing_tetrmino();



            int i, j;
            /* 今の位置を入れる */
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    /* 0も配列に入れているからエラーが起きる */
                    /* ので1が入っているもののみ代入する */
                    if (move_tetrmino[i, j] == 1)
                    {
                        field_tery[(i + y), (j + x)] = move_tetrmino[i, j];
                    }
                }
            }

            /* 動かすテトリスを描画するための処理*/
            for (i = 2; i < row; i++)
            {
                for (j = 0; j < col; j++)
                {
                    if ((field_tery[i, j]) == 1)
                    {
                        Point p = new Point();

                        p.Y = (i - 2) * 25;
                        p.X = j * 25;

                        tetr_ls.Add(p);
                        /* 描画・fm_Paintイベントに */
                        this.Invalidate();
                    }
                }
            }

            // 補助線あり
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    if (move_tetrmino[i, j] == 1)
                    {

                        Point p = new Point();

                        p.Y = (i + y2 -2) * 25;
                        p.X = (j + x) * 25;


                        landing_tetr_ls.Add(p);
                        /* 描画・fm_Paintイベントに */
                        this.Invalidate();

                    }
                }

            }



        }

        /* 新しいミノ作成 */
        public void New_tetrmino()
        {

            int i, j;
            int next = 0;

            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    move_tetrmino[i, j] = next_tetrmino[i, j];
                }
            }

            Next_tetymino();

            Move_tetrmino();

            for (int cnt = 0; cnt < 2; cnt++)
            {
                /* ひとつしたのブロックに地形が存在している場合、NEXTに1を立てる */
                for (i = 0; i < (row - 1); i++)
                {
                    for (j = 0; j < col; j++)
                    {
                        if ((field_tery[i, j] == 1) && (terrain_tetmino[(i + 1), j] == 1))
                        {
                            next = 1;
                        }
                    }
                }

                if (next == 1)
                {

                    New_terrain();
                    Extinguish_terrain();

                }
                else
                {
                    y += 1;
                    Move_tetrmino();
                }

            }
        }

        /* NEXT作成・レベル変更 */
        public void Next_tetymino()
        {

            /* ランダムに選択されたテトリミノを作成 */
            Tetrimino mino = new Tetrimino();

            int i, j;

            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    next_tetrmino[i, j] = mino.tetmino[i, j];
                }
            }

            /* ゲームオーバー画面だと変更できない */
            if (state == tetris_state.moving)
            {
                Next_draw?.Invoke(this, EventArgs.Empty);

                /* LEVELは200まで */

                if (g < 101)
                {
                    g += 1;
                    Level_draw?.Invoke(this, EventArgs.Empty);
                }
                if (100 < g　&&　g < 200)
                {
                    g_cnt++;
                    if (g_cnt == 3)
                    {
                        g += 1;
                        Level_draw?.Invoke(this, EventArgs.Empty);

                        g_cnt = 0;
                    }
                }
            }
        }

        /* テトリミノの地形を作成 */
        public void New_terrain()
        {
            int i, j;

            /* 今の位置を入れる */
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    /* 0も配列に入れているからエラーが起きる */
                    /* ので1が入っているもののみ代入する */
                    if (move_tetrmino[i, j] == 1)
                    {
                        terrain_tetmino[(i + y), (j + x)] = move_tetrmino[i, j];
                    }
                }
            }


            for (i = 2; i < row; i++)
            {
                for (j = 0; j < col; j++)
                {
                    if ((terrain_tetmino[i, j]) == 1)
                    {
                        Point p = new Point();

                        p.Y = (i - 2) * 25;
                        p.X = j * 25;

                        terrain_ls.Add(p);
                        /* 描画・fm_Paintイベントに */
                        this.Invalidate();
                    }
                }
            }
        }

        /* フィールドをクリア */
        public void Field_clear()
        {
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    field_tery[i, j] = 0;
                }
            }
        }

        /* 地形クリア　*/
        public void Terrain_clear()
        {
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    terrain_tetmino[i, j] = 0;
                }
            }

        }

        /* ホールドクリア */
        public void Hold_clear()
        {
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    hold_tetrmino[i, j] = 0;
                }
            }
        }

        /* テトリミノの回転 */
        public void Rotate_tetymino()
        {
            int[,] tentative_tetrmino =
            {
                {0, 0, 0, 0 },
                {0, 0, 0, 0 },
                {0, 0, 0, 0 },
                {0, 0, 0, 0 }
            };

            int i, j;
            int provisional_x = x;

            int check = 0;

            /* I */
            if ((move_tetrmino[2, 0] == 1) && (move_tetrmino[2, 1] == 1) &&
                (move_tetrmino[2, 2] == 1) && (move_tetrmino[2, 3] == 1))
            {
                tentative_tetrmino[0, 1] = 1;
                tentative_tetrmino[1, 1] = 1;
                tentative_tetrmino[2, 1] = 1;
                tentative_tetrmino[3, 1] = 1;
            }
            /* I */
            else if ((move_tetrmino[0, 1] == 1) && (move_tetrmino[1, 1] == 1) &&
                      (move_tetrmino[2, 1] == 1) && (move_tetrmino[3, 1] == 1))
            {
                tentative_tetrmino[2, 0] = 1;
                tentative_tetrmino[2, 1] = 1;
                tentative_tetrmino[2, 2] = 1;
                tentative_tetrmino[2, 3] = 1;
            }
            /* O(変更なし) */
            else if ((move_tetrmino[2, 1] == 1) && (move_tetrmino[2, 2] == 1) &&
                    (move_tetrmino[3, 1] == 1) && (move_tetrmino[3, 2] == 1))
            {
                check = 1;
            }
            else
            {
                /* 回転させる処理を仮の配列に */
                for (i = 0; i < 4; i++)
                {
                    for (j = 0; j < 4; j++)
                    {
                        tentative_tetrmino[i, j] = move_tetrmino[(3 - j), i];
                    }
                }

                /* 下が１行空白の場合詰める */
                while (true)
                {
                    if (tentative_tetrmino[3, 0] == 0 && tentative_tetrmino[3, 1] == 0 &&
                        tentative_tetrmino[3, 2] == 0 && tentative_tetrmino[3, 3] == 0)
                    {
                        for (i = 3; 0 < i; i--)
                        {
                            for (j = 0; j < 4; j++)
                            {
                                tentative_tetrmino[i, j] = tentative_tetrmino[(i - 1), j];
                            }
                        }

                        /* 詰めた分0を入れる */
                        for (i = 0; i < 4; i++)
                        {
                            tentative_tetrmino[0, i] = 0;
                        }
                    }
                    else
                    {
                        break;
                    }
                }

                /* 左が一行空白 */
                while (true)
                {
                    if (tentative_tetrmino[0, 0] == 0 && tentative_tetrmino[1, 0] == 0 &&
                       tentative_tetrmino[2, 0] == 0 && tentative_tetrmino[3, 0] == 0)
                    {
                        for (i = 0; i < 4; i++)
                        {
                            for (j = 0; j < 3; j++)
                            {
                                tentative_tetrmino[i, j] = tentative_tetrmino[i, (j + 1)];
                            }
                        }

                        for (i = 0; i < 4; i++)
                        {
                            tentative_tetrmino[i, 3] = 0;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }

            /* 横の壁に重なっている場合ずらす */
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    if (tentative_tetrmino[i, j] == 1)
                    {
                        for (; (j + provisional_x) > (col - 1); provisional_x--)
                        {

                        }
                        for (; ((j + provisional_x) < 0); provisional_x++)
                        {

                        }
                    }
                }
            }

            /* 回転できるかチェック */
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    if ((i + y) >= row)
                    {
                        check = 1;
                    }
                    else if ((tentative_tetrmino[i, j] == 1) && (terrain_tetmino[(i + y), (j + provisional_x)] == 1))
                    {
                        check = 1;
                    }
                }
                    
                
            }


            /* 回転可能なら回転させる */
            if (check == 0)
            {
                x = provisional_x;

                for (i = 0; i < 4; i++)
                {
                    for (j = 0; j < 4; j++)
                    {
                        move_tetrmino[i, j] = tentative_tetrmino[i, j];
                    }
                }

                rotate_cnt++;
            }
        }


        /* ホールド用のテトリミノの回転 */
        public void Hold_rotate_tetymino()
        {
            int[,] tentative_tetrmino =
            {
                {0, 0, 0, 0 },
                {0, 0, 0, 0 },
                {0, 0, 0, 0 },
                {0, 0, 0, 0 }
            };

            int i, j;
            int provisional_x = x;

            /* I */
            if ((move_tetrmino[2, 0] == 1) && (move_tetrmino[2, 1] == 1) &&
                (move_tetrmino[2, 2] == 1) && (move_tetrmino[2, 3] == 1))
            {
                tentative_tetrmino[0, 1] = 1;
                tentative_tetrmino[1, 1] = 1;
                tentative_tetrmino[2, 1] = 1;
                tentative_tetrmino[3, 1] = 1;
            }
            /* I */
            else if ((move_tetrmino[0, 1] == 1) && (move_tetrmino[1, 1] == 1) &&
                      (move_tetrmino[2, 1] == 1) && (move_tetrmino[3, 1] == 1))
            {
                tentative_tetrmino[2, 0] = 1;
                tentative_tetrmino[2, 1] = 1;
                tentative_tetrmino[2, 2] = 1;
                tentative_tetrmino[2, 3] = 1;
            }

            else
            {
                /* 回転させる処理を仮の配列に */
                for (i = 0; i < 4; i++)
                {
                    for (j = 0; j < 4; j++)
                    {
                        tentative_tetrmino[i, j] = move_tetrmino[(3 - j), i];
                    }
                }

                /* 下が１行空白の場合詰める */
                while (true)
                {
                    if (tentative_tetrmino[3, 0] == 0 && tentative_tetrmino[3, 1] == 0 &&
                        tentative_tetrmino[3, 2] == 0 && tentative_tetrmino[3, 3] == 0)
                    {
                        for (i = 3; 0 < i; i--)
                        {
                            for (j = 0; j < 4; j++)
                            {
                                tentative_tetrmino[i, j] = tentative_tetrmino[(i - 1), j];
                            }
                        }

                        /* 詰めた分0を入れる */
                        for (i = 0; i < 4; i++)
                        {
                            tentative_tetrmino[0, i] = 0;
                        }
                    }
                    else
                    {
                        break;
                    }
                }

                /* 左が一行空白 */
                while (true)
                {
                    if (tentative_tetrmino[0, 0] == 0 && tentative_tetrmino[1, 0] == 0 &&
                       tentative_tetrmino[2, 0] == 0 && tentative_tetrmino[3, 0] == 0)
                    {
                        for (i = 0; i < 4; i++)
                        {
                            for (j = 0; j < 3; j++)
                            {
                                tentative_tetrmino[i, j] = tentative_tetrmino[i, (j + 1)];
                            }
                        }

                        for (i = 0; i < 4; i++)
                        {
                            tentative_tetrmino[i, 3] = 0;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }

            /* 横の壁に重なっている場合ずらす */
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    if (tentative_tetrmino[i, j] == 1)
                    {
                        for (; (j + provisional_x) > (col - 1); provisional_x--)
                        {

                        }
                        for (; ((j + provisional_x) < 0); provisional_x++)
                        {

                        }
                    }
                }
            }

            x = provisional_x;

            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    move_tetrmino[i, j] = tentative_tetrmino[i, j];
                }
            }

            rotate_cnt++;
        }


        /* 一番下まで持っていく */
        public void Auto_tetrmino()
        {
            Landing_tetrmino();

            time_cnt = 0;
            hold_cnt = 0;
            rotate_cnt = 1;

            /* 落とした長さによって追加得点 */
            score += (y2 - y) * 10;

            Score_draw?.Invoke(this, EventArgs.Empty);

            y = y2;

            New_terrain();
            Move_tetrmino();

            this.Update();

            Extinguish_terrain();

            x = 3;
            y = -2;

            New_tetrmino();

            Move_tetrmino();
        }

        /* 一番下まで持っていく */
        public void Landing_tetrmino()
        {
            int next = 0;
            int i, j;

            y2 = y; 

            /* ひとつしたのブロックに地形が存在している場合、NEXTに1を立てる */
            while (next == 0)
            {
                /* フィールドの一番下 */
                for (i = 0; i < 4; i++)
                {
                    for (j = 0; j < 4; j++)
                    {
                        if (move_tetrmino[i, j] == 1)
                        {
                            if ((i + y2) == (row - 1))
                            {
                                next = 1;
                            }
                        }
                    }
                }

                /* 1つ下に地形がある */
                if (next == 0)
                {
                    for (i = 0; i < 4; i++)
                    {
                        for (j = 0; j < 4; j++)
                        {
                            if ((move_tetrmino[i, j] == 1) && (terrain_tetmino[(i + y2 + 1), (j + x)] == 1))
                            {
                                next = 1;
                            }
                        }
                    }
                }

                if (next == 0)
                {
                    y2 += 1;
                }
            }
        }

        /* 描画 */
        public void Fm_Paint(Object sender, PaintEventArgs e)
        {
            //グラフィックオブジェクトの取得
            Graphics g = e.Graphics;

            Pen dp_2 = new Pen(color2, 3);
            Pen dp_3 = new Pen(color3, 3);
            Pen dp_3_2 = new Pen(color3, 3);
            Pen dp_4 = new Pen(color4, 3);

            SolidBrush b_1 = new SolidBrush(color1);
            SolidBrush b_2 = new SolidBrush(color2);
            SolidBrush b_3 = new SolidBrush(color3);



            //着地
            foreach (Point p in landing_tetr_ls)
            {
                int x = p.X;
                int y = p.Y;
                g.FillRectangle(b_2, (x + 22), (y + 3), 25, 25);
//                g.DrawRectangle(dp_3, (x + 22), (y + 3), 25, 25);
            }

            foreach (Point p in tetr_ls)
            {
                int x = p.X;
                int y = p.Y;
                g.FillRectangle(b_3, (x + 22), (y + 3), 25, 25);
                g.DrawRectangle(dp_4, (x + 22), (y + 3), 25, 25);
            }

            if (state != tetris_state.moving)
            {
                foreach (Point p in terrain_ls)
                {
                    int x = p.X;
                    int y = p.Y;
                    g.FillRectangle(b_2, (x + 22), (y + 3), 25, 25);
                    g.DrawRectangle(dp_2, (x + 22), (y + 3), 25, 25);
                }
            }
            else
            {
                foreach (Point p in terrain_ls)
                {
                    int x = p.X;
                    int y = p.Y;
                    g.FillRectangle(b_2, (x + 22), (y + 3), 25, 25);
                    g.DrawRectangle(dp_4, (x + 22), (y + 3), 25, 25);
                }
            }

            foreach (Point p in Tisappear_terrain_ls)
            {
                int x = p.X;
                int y = p.Y;
                g.FillRectangle(b_1, (x + 22), (y + 3), 25, 25);
                g.DrawRectangle(dp_3, (x + 22), (y + 3), 25, 25);
            }



            //形
            g.FillRectangle(b_3, 0, 0, 20, 600);
            g.FillRectangle(b_1, 0, 0, 5, 600);
            g.FillRectangle(b_1, 8, 0, 3, 600);

            g.FillRectangle(b_3, (this.Width - 20), 0, 20, 600);
            g.FillRectangle(b_1, (this.Width - 5), 0, 5, 600);
            g.FillRectangle(b_1, (this.Width - 11), 0, 3, 600);

            //g.DrawRectangle(dp, 10, 0, (this.Width - 20), (this.Height - 20));
            //g.DrawRectangle(dp4, 13, 0, (this.Width - 26), (this.Height - 26));
            this.Focus();
        }


        /* ホールド */
        public void chang_hold()
        {
            /* ホールドできるのは地形作るまでに一回まで */
            if (hold_cnt == 0)
            {
//                timer1.Enabled = false;

                int chack = 0;

                for (int i = 0; i < 4; i++)
                {
                    for (int j = 0; j < 4; j++)
                    {
                        if (hold_tetrmino[i, j] == 1)
                        {
                            chack = 1;
                        }
                    }
                }

                /* ホールドに何かある場合 */
                if (chack == 1)
                {

                    /* 回転していたら元に戻す */
                    for (; (rotate_cnt % 4 != 1);)
                    {
                        /* この場合、回転できなくても回転させる */
                        Hold_rotate_tetymino();
                    }

                    int[,] provisional_tetrmino = new int[4, 4];

                    for (int i = 0; i < 4; i++)
                    {
                        for (int j = 0; j < 4; j++)
                        {
                            provisional_tetrmino[i, j] = hold_tetrmino[i, j];
                            hold_tetrmino[i, j] = move_tetrmino[i, j];
                            move_tetrmino[i, j] = provisional_tetrmino[i, j];
                        }
                    }

                    time_cnt = 0;

                    x = 3;
                    y = -2;

                    int next = 0;

                    Move_tetrmino();

                    /* ホールドで持ってきたテトリミノを表示させる */
                    for (int cnt = 0; cnt < 2; cnt++)
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            for (int j = 2; j < 4; j++)
                            {
                                if ((field_tery[i, j] == 1) && (terrain_tetmino[(i + 1), j] == 1))
                                {
                                    next = 1;
                                }
                            }
                        }

                        if (next == 1)
                        {

                            New_terrain();
                            Extinguish_terrain();

                        }
                        else
                        {
                            y += 1;
                            Move_tetrmino();
                        }

                    }

                }
                else
                {
                    /* 回転していたら元に戻す */
                    for (; (rotate_cnt % 4 != 1);)
                    {
                        /* この場合、回転できなくても回転させる */
                        Hold_rotate_tetymino();
                    }

                    for (int i = 0; i < 4; i++)
                    {
                        for (int j = 0; j < 4; j++)
                        {
                            hold_tetrmino[i, j] = move_tetrmino[i, j];
                        }
                    }

                    time_cnt = 0;

                    x = 3;
                    y = -2;

                    New_tetrmino();

                    Move_tetrmino();
                }

                Hold_draw?.Invoke(this, EventArgs.Empty);

                time_cnt = 0;
                hold_cnt = 1;
                rotate_cnt = 1;

//                timer1.Enabled = true;
            }
        }

        private void under_chack()
        {
            int i, j;

            int under_mino_chack = 0;
            int under_wall_chack = 0;

            /* ひとつしたのブロックに地形が存在している場合、under_minoに1を立てる */
            for (i = 0; i < (row - 1); i++)
            {
                for (j = 0; j < col; j++)
                {
                    if ((field_tery[i, j] == 1) && (terrain_tetmino[(i + 1), j] == 1))
                    {
                        under_mino_chack = 1;
                    }
                }
            }

            under_mino = under_mino_chack;

            /* 1つ下が壁だった場合under_wallに1立てる */
            for (i = 0; i < row; i++)
            {
                for (j = 0; j < col; j++)
                {
                    if (field_tery[i, j] == 1)
                    {
                        if (i == (row - 1))
                        {
                            under_wall_chack = 1;
                        }
                    }
                }
            }

            under_wall = under_wall_chack;
        }

        private void Extinguish_terrain()
        {
            int i = 0;
            int j = 0;
            int k = 0;

            int cnt = 0;

            /* テトリミノを消す */
            for (i = 0; i < row; i++)
            {
                if ((terrain_tetmino[i, 0] == 1) && (terrain_tetmino[i, 1] == 1) &&
                     (terrain_tetmino[i, 2] == 1) && (terrain_tetmino[i, 3] == 1) &&
                     (terrain_tetmino[i, 4] == 1) && (terrain_tetmino[i, 5] == 1) &&
                     (terrain_tetmino[i, 6] == 1) && (terrain_tetmino[i, 7] == 1) &&
                     (terrain_tetmino[i, 8] == 1) && (terrain_tetmino[i, 9] == 1))

                {

                    //                    timer1.Enabled = false;

                    cnt += 1;

                    for (j = 0; j < col; j++)
                    {
                        if ((terrain_tetmino[i, j]) == 1)
                        {
                            Point p = new Point();

                            p.Y = (i - 2) * 25;
                            p.X = j * 25;

                            Tisappear_terrain_ls.Add(p);
                            /* 描画・fm_Paintイベントに */
                            this.Invalidate();

                        }
                    }

                    /* 消す行を詰める */
                    for (j = i; 0 < j; j--)
                    {
                        for (k = 0; k < col; k++)
                        {
                            terrain_tetmino[j, k] = terrain_tetmino[(j - 1), k];
                        }
                    }

                }

            }

            if (0 < cnt)
            {
                this.Update();
                System.Threading.Thread.Sleep(300);


                /* 詰めた分0を入れる */
                for (i = 0; i < cnt; i++)
                {
                    for (j = 0; j < col; j++)
                    {
                        terrain_tetmino[i, j] = 0;
                    }
                }

                Tisappear_terrain_ls.Clear();
                terrain_ls.Clear();

                for (i = 0; i < row; i++)
                {
                    for (j = 0; j < col; j++)
                    {
                        if ((terrain_tetmino[i, j]) == 1)
                        {
                            Point p = new Point();

                            p.Y = (i - 2) * 25;
                            p.X = j * 25;

                            terrain_ls.Add(p);
                            /* 描画・fm_Paintイベントに */
                            this.Invalidate();

                        }
                    }
                }

                //スコア
                score += (cnt * cnt * (cnt - 1) * 100) + (150 * difficulty * (difficulty-1));

                if (g < 101)
                {
                    g += cnt;
                    Level_draw?.Invoke(this, EventArgs.Empty);
                }

                Score_draw?.Invoke(this, EventArgs.Empty);

                //                timer1.Enabled = true;

            }

        }

        /* タイマー */
        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Focus();

            time_cnt += time_cnt_value;

            under_chack();

            /* 下に地形がある場合、新しいテトリミノを作成 */
            if ((under_mino == 1) || (under_wall == 1))
            {
                terrain_time_cnt += 1;

                if (terrai_time_cnt_value < terrain_time_cnt)
                {

//                    timer1.Enabled = false;

                    New_terrain();

                    Extinguish_terrain();

                    x = 3;
                    y = -2;

                    hold_cnt = 0;
                    rotate_cnt = 1;

                    New_tetrmino();

                    Move_tetrmino();

                    under_mino = 0;
                    under_wall = 0;
                    terrain_time_cnt = 0;

                    time_cnt = 0;

//                    timer1.Enabled = true;

                }
            }

            /* 落ちる処理 */
            if ((time_cnt + (g * 2)) > 429)
            {
                /* 下にミノと壁がない場合 */
                if ((under_mino != 1) && (under_wall != 1))
                {
                    y += 1;
                    Move_tetrmino();
                }
                time_cnt = 0;

            }

            /* ゲームオーバー */
            if ((terrain_tetmino[1, 3] == 1) || (terrain_tetmino[1, 4] == 1) ||
                 (terrain_tetmino[1, 5] == 1) || (terrain_tetmino[1, 6] == 1))
            {
//                timer1.Enabled = false;
                gameover_lbl.Visible = true;
                gameover_lbl2.Visible = true;

                state = tetris_state.stop;

                if (high_score < score)
                {
                    high_score = score;
                    High_score_draw?.Invoke(this, EventArgs.Empty);
                }

                this.Invalidate();
                landing_tetr_ls.Clear();
            }

            this.Focus();
        }

        public event EventHandler Next_draw;
        public event EventHandler End;
        public event EventHandler Score_draw;
        public event EventHandler High_score_draw;
        public event EventHandler Level_draw;
        public event EventHandler Hold_draw;

    }
}
