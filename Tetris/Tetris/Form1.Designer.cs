﻿namespace Tetris
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.field = new Tetris.Field();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.Next_tetymino = new Tetris.Next_tetymino();
            this.Next_lbl = new System.Windows.Forms.Label();
            this.Score_lbl = new System.Windows.Forms.Label();
            this.Level = new System.Windows.Forms.Label();
            this.Score = new System.Windows.Forms.Label();
            this.Level_lbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // field
            // 
            this.field.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(233)))), ((int)(((byte)(204)))));
            this.field.Location = new System.Drawing.Point(50, 0);
            this.field.Margin = new System.Windows.Forms.Padding(0);
            this.field.Name = "field";
            this.field.Size = new System.Drawing.Size(250, 500);
            this.field.TabIndex = 0;
            this.field.Next_draw += new System.EventHandler(this.field_Next_draw);
            this.field.Score_draw += new System.EventHandler(this.filed_Score_draw);
            this.field.Level_draw += new System.EventHandler(this.field_Level_draw);
            // 
            // Next_tetymino
            // 
            this.Next_tetymino.BackColor = System.Drawing.Color.Black;
            this.Next_tetymino.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Next_tetymino.Location = new System.Drawing.Point(341, 112);
            this.Next_tetymino.Margin = new System.Windows.Forms.Padding(0);
            this.Next_tetymino.Name = "Next_tetymino";
            this.Next_tetymino.Size = new System.Drawing.Size(122, 72);
            this.Next_tetymino.TabIndex = 3;
            // 
            // Next_lbl
            // 
            this.Next_lbl.AutoSize = true;
            this.Next_lbl.BackColor = System.Drawing.Color.Transparent;
            this.Next_lbl.Font = new System.Drawing.Font("Nirmala UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Next_lbl.ForeColor = System.Drawing.Color.White;
            this.Next_lbl.Location = new System.Drawing.Point(382, 75);
            this.Next_lbl.Name = "Next_lbl";
            this.Next_lbl.Size = new System.Drawing.Size(81, 37);
            this.Next_lbl.TabIndex = 4;
            this.Next_lbl.Text = "NEXT";
            // 
            // Score_lbl
            // 
            this.Score_lbl.BackColor = System.Drawing.Color.Transparent;
            this.Score_lbl.Font = new System.Drawing.Font("MS UI Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Score_lbl.ForeColor = System.Drawing.Color.White;
            this.Score_lbl.Location = new System.Drawing.Point(369, 257);
            this.Score_lbl.Name = "Score_lbl";
            this.Score_lbl.Size = new System.Drawing.Size(101, 21);
            this.Score_lbl.TabIndex = 5;
            this.Score_lbl.Text = "0";
            this.Score_lbl.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Level
            // 
            this.Level.AutoSize = true;
            this.Level.Font = new System.Drawing.Font("Nirmala UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Level.ForeColor = System.Drawing.Color.White;
            this.Level.Location = new System.Drawing.Point(382, 291);
            this.Level.Name = "Level";
            this.Level.Size = new System.Drawing.Size(88, 37);
            this.Level.TabIndex = 7;
            this.Level.Text = "LEVEL";
            // 
            // Score
            // 
            this.Score.AutoSize = true;
            this.Score.Font = new System.Drawing.Font("Nirmala UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Score.ForeColor = System.Drawing.Color.White;
            this.Score.Location = new System.Drawing.Point(372, 207);
            this.Score.Name = "Score";
            this.Score.Size = new System.Drawing.Size(98, 37);
            this.Score.TabIndex = 6;
            this.Score.Text = "SCORE";
            // 
            // Level_lbl
            // 
            this.Level_lbl.Font = new System.Drawing.Font("MS UI Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Level_lbl.ForeColor = System.Drawing.Color.White;
            this.Level_lbl.Location = new System.Drawing.Point(369, 345);
            this.Level_lbl.Name = "Level_lbl";
            this.Level_lbl.Size = new System.Drawing.Size(101, 21);
            this.Level_lbl.TabIndex = 10;
            this.Level_lbl.Text = "0";
            this.Level_lbl.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(479, 502);
            this.Controls.Add(this.Level_lbl);
            this.Controls.Add(this.Level);
            this.Controls.Add(this.Score);
            this.Controls.Add(this.Score_lbl);
            this.Controls.Add(this.Next_lbl);
            this.Controls.Add(this.field);
            this.Controls.Add(this.Next_tetymino);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public Field field;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label Next_lbl;
        public Next_tetymino Next_tetymino;
        private System.Windows.Forms.Label Score_lbl;
        private System.Windows.Forms.Label Score;
        private System.Windows.Forms.Label Level;
        private System.Windows.Forms.Label Level_lbl;
    }
}

