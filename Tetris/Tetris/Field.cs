﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Tetris
{
    public partial class Field : UserControl
    {
        /* 動かすテトリミノ */
        public static int[,] move_tetrmino = new int[4, 4];
        /* 次のテトリミノ */
        public static int[,] next_tetrmino = new int[4, 4];
        /* フィールド定義 */
        public static int[,] field_tery = new int[22, 10];
        /* テトリミノの地形 */
        public static int[,] terrain_tetmino = new int[22, 10];

        public static int score = 0;
        public static int level = 0;

        int level_cnt = 1;

        /* フィールドの大きさ */
        public int col = 10;
        public int row = 22;

        /* 操作するテトリミノを表す添え字 */
        int y = -2;      //高さ
        int x = 0;      //横

        int game_over_antenna = 0;

        /* 動かすテトリミノのリスト */
        private List<Point> tetr_ls = new List<Point>();

        /* 地形のリスト */
        private List<Point> terrain_ls = new List<Point>();


        public Field()
        {
            InitializeComponent();

            this.DoubleBuffered = true; //ダブルバッファ・ちらつき防止


        }

        private void Field_Load(object sender, EventArgs e)
        {
            Next_tetymino();
            New_tetrmino();

            Move_tetrmino();

            timer1.Enabled = true;
            timer2.Enabled = true;  //タイマースタート
            this.DoubleBuffered = true; //ダブルバッファ・ちらつき防止

            gameover_lbl.Left = (this.Width / 2) - (gameover_lbl.Width / 2);
            gameover_lbl.Top = (this.Height / 2) - (gameover_lbl.Height / 2); //ggameoverラベルの位置を画面中央に
            gameover_lbl.Visible = false;

            this.Paint += new PaintEventHandler(Fm_Paint);
        }

        /* テトリミノの位置を移動 */
        public void Move_tetrmino()
        {
            /* 今までの描画とフィールドをクリア */
            tetr_ls.Clear();
            Field_clear();


            int i, j;
            /* 今の位置を入れる */
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    /* 0も配列に入れているからエラーが起きる */
                    /* ので1が入っているもののみ代入する */
                    if (move_tetrmino[i, j] == 1)
                    {
                        field_tery[(i + y), (j + x)] = move_tetrmino[i, j];
                    }
                }
            }

            /* 動かすテトリスを描画するための処理*/
            for (i = 2; i < row; i++)
            {
                for (j = 0; j < col; j++)
                {
                    if ((field_tery[i, j]) == 1)
                    {
                        Point p = new Point();

                        p.Y = (i - 2) * 25;
                        p.X = j * 25;

                        tetr_ls.Add(p);
                        /* 描画・fm_Paintイベントに */
                        this.Invalidate();

                    }
                }

            }
        }

        /* 新しいミノ作成 */
        public void New_tetrmino()
        {

            int i, j;
            int next = 0;

            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    move_tetrmino[i, j] = next_tetrmino[i, j];
                }
            }

            Next_tetymino();

            x = 3;

            Move_tetrmino();

            for (int cnt = 0; cnt < 2; cnt++)
            {
                /* ひとつしたのブロックに地形が存在している場合、NEXTに1を立てる */
                for (i = 0; i < (row - 1); i++)
                {
                    for (j = 0; j < col; j++)
                    {
                        if ((field_tery[i, j] == 1) && (terrain_tetmino[(i + 1), j] == 1))
                        {
                            next = 1;
                        }
                    }
                }

                /* 下に地形がある場合、新しいテトリミノを作成 */
                if (next == 1)
                {

                    New_terrain();

                }
                else
                {
                    y += 1;
                    Move_tetrmino();
                }

            }
        }

        /* NEXT作成・レベル変更 */
        public void Next_tetymino()
        {

            /* ランダムに選択されたテトリミノを作成 */
            Tetrimino mino = new Tetrimino();

            int i, j;

            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    next_tetrmino[i, j] = mino.tetmino[i, j];
                }
            }

            /* ゲームオーバー画面だと変更できない */
            if (game_over_antenna == 0)
            {
                Next_draw?.Invoke(this, EventArgs.Empty);

                level_cnt += 1;
                /* LEVELは20まで */
                if ((level != 10)  && ((level_cnt % 20) == 0))
                {
                    level += 1;
                    timer1.Interval -= 100;
                    Level_draw?.Invoke(this, EventArgs.Empty);
                }
            }
            //OnNext_draw(EventArgs.Empty);
        }


        /* テトリミノの地形を作成 */
        public void New_terrain()
        {
            int i, j;

            /* 今の位置を入れる */
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    /* 0も配列に入れているからエラーが起きる */
                    /* ので1が入っているもののみ代入する */
                    if (move_tetrmino[i, j] == 1)
                    {
                        terrain_tetmino[(i + y), (j + x)] = move_tetrmino[i, j];
                    }
                }
            }


            for (i = 2; i < row; i++)
            {
                for (j = 0; j < col; j++)
                {
                    if ((terrain_tetmino[i, j]) == 1)
                    {
                        Point p = new Point();

                        p.Y = (i - 2) * 25;
                        p.X = j * 25;

                        terrain_ls.Add(p);
                        /* 描画・fm_Paintイベントに */
                        this.Invalidate();

                    }
                }
            }
        }

        /* フィールドをクリア */
        public void Field_clear()
        {
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    field_tery[i, j] = 0;
                }
            }
        }

        /* 地形クリア　*/
        public void Terrain_clear()
        {
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    terrain_tetmino[i, j] = 0;
                }
            }
        }

        /* テトリミノの回転 */
        public void Rotate_tetymino()
        {
            int[,] tentative_tetrmino =
            {
                {0, 0, 0, 0 },
                {0, 0, 0, 0 },
                {0, 0, 0, 0 },
                {0, 0, 0, 0 }
            };

            int i, j;

            int check = 0;

            if ((move_tetrmino[2, 0] == 1) && (move_tetrmino[2, 1] == 1) &&
                (move_tetrmino[2, 2] == 1) && (move_tetrmino[2, 3] == 1))
            {
                tentative_tetrmino[0, 1] = 1;
                tentative_tetrmino[1, 1] = 1;
                tentative_tetrmino[2, 1] = 1;
                tentative_tetrmino[3, 1] = 1;
            }
            else if ((move_tetrmino[0, 1] == 1) && (move_tetrmino[1, 1] == 1) &&
                      (move_tetrmino[2, 1] == 1) && (move_tetrmino[3, 1] == 1))
            {
                tentative_tetrmino[2, 0] = 1;
                tentative_tetrmino[2, 1] = 1;
                tentative_tetrmino[2, 2] = 1;
                tentative_tetrmino[2, 3] = 1;
            }
            else if ((move_tetrmino[2, 1] == 1) && (move_tetrmino[2, 2] == 1) &&
                    (move_tetrmino[3, 1] == 1) && (move_tetrmino[3, 2] == 1))
            {
                check = 1;
            }
            else
            {
                /* 回転させる処理を仮の配列に */
                for (i = 0; i < 4; i++)
                {
                    for (j = 0; j < 4; j++)
                    {
                        tentative_tetrmino[i, j] = move_tetrmino[(3 - j), i];
                    }
                }

                /* 下が１行空白の場合詰める */
                while (true)
                {
                    if (tentative_tetrmino[3, 0] == 0 && tentative_tetrmino[3, 1] == 0 &&
                        tentative_tetrmino[3, 2] == 0 && tentative_tetrmino[3, 3] == 0)
                    {
                        for (i = 3; 0 < i; i--)
                        {
                            for (j = 0; j < 4; j++)
                            {
                                tentative_tetrmino[i, j] = tentative_tetrmino[(i - 1), j];
                            }
                        }

                        /* 詰めた分0を入れる */
                        for (i = 0; i < 4; i++)
                        {
                            tentative_tetrmino[0, i] = 0;
                        }
                    }
                    else
                    {
                        break;
                    }
                }

                /* 左が一行空白 */
                while (true)
                {
                    if (tentative_tetrmino[0, 0] == 0 && tentative_tetrmino[1, 0] == 0 &&
                       tentative_tetrmino[2, 0] == 0 && tentative_tetrmino[3, 0] == 0)
                    {
                        for (i = 0; i < 4; i++)
                        {
                            for (j = 0; j < 3; j++)
                            {
                                tentative_tetrmino[i, j] = tentative_tetrmino[i, (j + 1)];
                            }
                        }

                        for (i = 0; i < 4; i++)
                        {
                            tentative_tetrmino[i, 3] = 0;
                        }
                    }
                    else
                    {
                        break;
                    }
                }

            }

            /* 横の壁に重なっている場合ずらす */
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    if (tentative_tetrmino[i, j] == 1)
                    {
                        for (; (j + x) > (col - 1); x--)
                        {

                        }
                        for (; ((j + x) < 0); x++)
                        {

                        }
                    }
                }
            }

            /* 回転できるかチェック */
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    if ((tentative_tetrmino[i, j] == 1) && (terrain_tetmino[(i + y), (j + x)] == 1))
                    {
                        check = 1;
                    }
                }
            }


            /* 回転可能なら回転させる */
            if (check == 0)
            {
                for (i = 0; i < 4; i++)
                {
                    for (j = 0; j < 4; j++)
                    {
                        move_tetrmino[i, j] = tentative_tetrmino[i, j];
                    }
                }

            }

            Move_tetrmino();

        }

        /* 一番下まで持っていく */
        public void Auto_tetrmino()
        {
            timer1.Enabled = false;

            int next = 0;
            int i, j;

            /* ひとつしたのブロックに地形が存在している場合、NEXTに1を立てる */
            while (next == 0)
            {
                /* フィールドの一番下 */
                for (i = 0; i < 4; i++)
                {
                    for (j = 0; j < 4; j++)
                    {
                        if (move_tetrmino[i, j] == 1)
                        {
                            if ((i + y) == (row - 1))
                            {
                                next = 1;
                            }
                        }
                    }
                }

                /* 1つ下に地形がある */
                if (next == 0)
                {
                    for (i = 0; i < 4; i++)
                    {
                        for (j = 0; j < 4; j++)
                        {
                            if ((move_tetrmino[i, j] == 1) && (terrain_tetmino[(i + y + 1), (j + x)] == 1))
                            {
                                next = 1;
                            }
                        }
                    }
                }

                if (next == 0)
                {
                    y += 1;
                }
            }

            New_terrain();

            x = 0;
            y = -2;

            New_tetrmino();

            Move_tetrmino();

            timer1.Enabled = true;

        }

        /* 描画 */
        public void Fm_Paint(Object sender, PaintEventArgs e)
        {
            //グラフィックオブジェクトの取得
            Graphics g = e.Graphics;

            /* テトリミノ */
            Pen dp = new Pen(Color.LightGray, 1);
            SolidBrush b = new SolidBrush(Color.Blue);

            /* 地形 */
            Pen dp2 = new Pen(Color.LightGray, 1);
            SolidBrush b2 = new SolidBrush(Color.DarkGray);

            /////* Next */
            //Next_tetymino n = new Next_tetymino();

            /* フィールドに升目をつける */
            int y_line = 25;
            int x_line = 25;

            for (; y_line < 505; y_line += 25)
            {
                g.DrawLine(Pens.DimGray, 0, y_line, 255, y_line);
            }

            for (; x_line < 255; x_line += 25)
            {
                g.DrawLine(Pens.DimGray, x_line, 0, x_line, 505);
            }

            foreach (Point p in tetr_ls)
            {
                int x = p.X;
                int y = p.Y;
                g.FillRectangle(b, x, y, 25, 25);
                g.DrawRectangle(dp, x, y, 25, 25);
            }

            foreach (Point p in terrain_ls)
            {
                int x = p.X;
                int y = p.Y;
                g.FillRectangle(b2, x, y, 25, 25);
                g.DrawRectangle(dp2, x, y, 25, 25);
            }

            this.Focus();
        }

        /* キーが押された時 */
        private void Field_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            int down = 0;
            int r = 0;
            int l = 0;

            int next_dowm = 0;
            int next_r = 0;
            int next_l = 0;

            /* ぶつかっていない箇所を探す */
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    if (field_tery[i, j] == 1)
                    {
                        if (i == (row - 1))
                        {
                            //下ダメ
                            down = 1;
                        }
                        if (j == (col - 1))
                        {
                            //右ダメ
                            r = 1;
                        }
                        if (j == 0)
                        {
                            //左ダメ
                            l = 1;
                        }
                    }
                }
            }

            /* フィールド内かつ、動かす場所に地形がない場合、
             * テトリミノを動かす */
            if (e.KeyCode == Keys.Down)
            {
                for (int i = 0; i < (row - 1); i++)
                {
                    for (int j = 0; j < col; j++)
                    {
                        if ((field_tery[i, j] == 1) && (terrain_tetmino[(i + 1), j] == 1))
                        {
                            next_dowm = 1;
                        }
                    }
                }
                if ((next_dowm == 0) && (down == 0))
                {
                    y += 1;
                    Move_tetrmino();
                }

            }
            else if (e.KeyCode == Keys.Right)
            {
                for (int i = 0; i < row; i++)
                {
                    for (int j = 0; j < (col - 1); j++)
                    {
                        if ((field_tery[i, j] == 1) && (terrain_tetmino[i, (j + 1)] == 1))
                        {
                            next_r = 1;
                        }
                    }
                }
                if ((next_r == 0) && (r == 0))
                {
                    x += 1;
                    Move_tetrmino();
                }
            }
            else if (e.KeyCode == Keys.Left)
            {
                for (int i = 0; i < row; i++)
                {
                    for (int j = 1; j < col; j++)
                    {
                        if ((field_tery[i, j] == 1) && (terrain_tetmino[i, (j - 1)] == 1))
                        {
                            next_l = 1;
                        }
                    }
                }

                if ((next_l == 0) && (l == 0))
                {
                    x -= 1;
                    Move_tetrmino();
                }
            }
            else if (e.KeyCode == Keys.Space)
            {
                Rotate_tetymino();
            }
            else if (e.KeyCode == Keys.Z)
            {
                Auto_tetrmino();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                game_over_antenna = 0;

                tetr_ls.Clear();
                terrain_ls.Clear();

                Field_clear();
                Terrain_clear();

                y = -2;
                x = 0;

                score = 0;
                level = 0;
                Level_draw?.Invoke(this, EventArgs.Empty);


                Next_tetymino();
                New_tetrmino();

                Move_tetrmino();

                timer1.Enabled = true;
                timer2.Enabled = true;  //タイマースタート

                gameover_lbl.Visible = false;
            }
        }

        /* タイマー */
        private void timer1_Tick(object sender, EventArgs e)
        {
            int down = 0;
            int next = 0;


            /* ひとつしたのブロックに地形が存在している場合、NEXTに1を立てる */
            for (int i = 0; i < (row - 1); i++)
            {
                for (int j = 0; j < col; j++)
                {
                    if ((field_tery[i, j] == 1) && (terrain_tetmino[(i + 1), j] == 1))
                    {
                        next = 1;
                    }
                }
            }

            /* 下に地形がある場合、新しいテトリミノを作成 */
            if (next == 1)
            {

                timer1.Enabled = false;

                New_terrain();


                x = 0;
                y = -2;

                New_tetrmino();

                Move_tetrmino();

                timer1.Enabled = true;

            }
            else
            {

                for (int i = 0; i < row; i++)
                {
                    for (int j = 0; j < col; j++)
                    {
                        if (field_tery[i, j] == 1)
                        {
                            if (i == (row - 1))
                            {
                                down = 1;
                            }
                        }
                    }
                }

                /* 1下げる */
                if (down == 0)
                {
                    y += 1;
                    Move_tetrmino();
                }
                /* 壁にぶつかった場合 */
                else
                {
                    timer1.Enabled = false;

                    New_terrain();


                    x = 0;
                    y = -2;

                    New_tetrmino();

                    Move_tetrmino();

                    timer1.Enabled = true;

                }
            }

        }

        private void timer2_Tick(object sender, EventArgs e)
        {

            int i = 0;
            int j = 0;
            int k = 0;

            int cnt = 0;
            
            /* テトリミノを消す */
            for (i = 0; i < row; i++)
            {
                if ((terrain_tetmino[i, 0] == 1) && (terrain_tetmino[i, 1] == 1) &&
                     (terrain_tetmino[i, 2] == 1) && (terrain_tetmino[i, 3] == 1) &&
                     (terrain_tetmino[i, 4] == 1) && (terrain_tetmino[i, 5] == 1) &&
                     (terrain_tetmino[i, 6] == 1) && (terrain_tetmino[i, 7] == 1) &&
                     (terrain_tetmino[i, 8] == 1) && (terrain_tetmino[i, 9] == 1))

                {
                    cnt += 1;
                    terrain_ls.Clear();

                    for (j = (row - 1); i < j; j--)
                    {
                        for (k = 0; k < col; k++)
                        {
                            if ((terrain_tetmino[j, k]) == 1)
                            {
                                Point p = new Point();

                                p.Y = (j - 2) * 25;
                                p.X = k * 25;

                                terrain_ls.Add(p);
                                /* 描画・fm_Paintイベントに */
                                this.Invalidate();

                            }
                        }
                    }
                    for (j = i; 0 < j; j--)
                    {
                        for (k = 0; k < col; k++)
                        {
                            terrain_tetmino[j, k] = terrain_tetmino[(j - 1), k];

                            if ((terrain_tetmino[j, k]) == 1)
                            {
                                Point p = new Point();

                                p.Y = (j - 2) * 25;
                                p.X = k * 25;

                                terrain_ls.Add(p);
                                /* 描画・fm_Paintイベントに */
                                this.Invalidate();

                            }
                        }
                    }

                    /* 詰めた分0を入れる */
                    for (j = 0; j < col; j++)
                    {
                        terrain_tetmino[0, j] = 0;
                    }

                    score += (cnt * 750) + 500;

                    Score_draw?.Invoke(this, EventArgs.Empty);
                }
            }

            /* ゲームオーバー */
            if ((terrain_tetmino[1, 3] == 1) || (terrain_tetmino[1, 4] == 1) ||
                 (terrain_tetmino[1, 5] == 1) || (terrain_tetmino[1, 6] == 1))
            {
                timer1.Enabled = false;
                gameover_lbl.Visible = true;
                game_over_antenna = 1;
            }
        }

        public event EventHandler Next_draw;
        public event EventHandler Score_draw;
        public event EventHandler Level_draw;

    }
}
