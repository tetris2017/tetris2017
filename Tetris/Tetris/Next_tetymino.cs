﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tetris
{
    public partial class Next_tetymino : UserControl
    {

        private static List<Point> next_tetr_ls = new List<Point>();


        public Next_tetymino()
        {
            InitializeComponent();

            //this.Paint += new PaintEventHandler(Fm_Paint);
        }

      
        public void Next_drawing(int[,] next)
        {

            next_tetr_ls.Clear();

            int i, j;

            for (i = 2; i < 4; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    if ((next[i, j]) == 1)
                    {
                        Point p = new Point();

                        p.Y = (i - 2) * 25 + 10;
                        p.X = j * 25 + 10;

                        next_tetr_ls.Add(p);
                        /* 描画・fm_Paintイベントに */
                        this.Invalidate();
                    }
                }
            }
        }


        private void Next_tetymino_Paint(object sender, PaintEventArgs e)
        {
            //グラフィックオブジェクトの取得
            Graphics g = e.Graphics;

            /* テトリミノ */
            Pen dp = new Pen(Color.Black, 1);
            SolidBrush b = new SolidBrush(Color.MediumSeaGreen);

            foreach (Point p in next_tetr_ls)
            {
                int x = p.X;
                int y = p.Y;
                g.FillRectangle(b, x, y, 25, 25);
                g.DrawRectangle(dp, x, y, 25, 25);
            }

        }
    }
}
