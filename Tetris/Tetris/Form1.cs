﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tetris
{


    public partial class Form1 : Form
    {

        public static int[,] a ={
                     { 1,0,1,0},
                     { 1,0,0,0},
                     { 1,0,0,0},
                     { 0,0,1,0}
                };

        public Form1()
        {
            InitializeComponent();
        }

        private void field_Next_draw(object sender, EventArgs e)
        {

            Next_tetymino.Next_drawing(Field.next_tetrmino);

        }

        private void filed_Score_draw(object sender, EventArgs e)
        {
            Score_lbl.Text = Convert.ToString(Field.score);
        }

        private void field_Level_draw(object sender, EventArgs e)
        {
            Level_lbl.Text = Convert.ToString(Field.level);
        }
    }
}
