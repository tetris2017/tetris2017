﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tetris
{


    public partial class Form1 : Form
    {
        Color color1 = Color.FromArgb(205, 221, 135);          //白
        Color color2 = Color.FromArgb(147, 168, 49);            //薄い黒
        Color color3 = Color.FromArgb(105, 118, 15);            //濃い黒
        Color color4 = Color.FromArgb(43, 58, 1);          //黒


        public Form1()
        {
            InitializeComponent();
        }

        private void field_Next_draw(object sender, EventArgs e)
        {

            Next_tetymino.Next_drawing(Field.next_tetrmino);
        }


        private void filed_Score_draw(object sender, EventArgs e)
        {
            Score_lbl.Text = Convert.ToString(Field.score);
        }

        private void field_Level_draw(object sender, EventArgs e)
        {
            Level_lbl.Text = Convert.ToString(Field.g);
        }

        private void field_Hold_draw(object sender, EventArgs e)
        {
            hold1.Hold_drawing(Field.hold_tetrmino);

        }

        private void field_End(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_Enter(object sender, EventArgs e)
        {
            field.Focus();
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            Pen dp = new Pen(color3, 15);
            Pen dp2 = new Pen(color2, 5);
            g.DrawRectangle(dp, 0, 0, (hold_panel.Width), (hold_panel.Height));
            g.DrawRectangle(dp2, 10, 10, (hold_panel.Width - 20), (hold_panel.Height - 20));

        }

        private void next_panel_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            Pen dp = new Pen(color3, 15);
            Pen dp2 = new Pen(color2, 5);
            g.DrawRectangle(dp, 0, 0, (next_panel.Width), (next_panel.Height));
            g.DrawRectangle(dp2, 10, 10, (next_panel.Width - 20), (next_panel.Height - 20));
        }

        private void score_panel_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            Pen dp = new Pen(color3, 15);
            Pen dp2 = new Pen(color2, 5);
            g.DrawRectangle(dp, 0, 0, (score_panel.Width), (score_panel.Height));
            g.DrawRectangle(dp2, 10, 10, (score_panel.Width - 20), (score_panel.Height - 20));


        }

        private void field_High_score_draw(object sender, EventArgs e)
        {
            High_score_lbl.Text = Convert.ToString(Field.high_score);
        }
    }
}
