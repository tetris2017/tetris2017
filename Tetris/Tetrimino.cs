﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tetris
{
    public partial class Tetrimino : UserControl
    {

        public int[,] tetmino = new int[4, 4];

        public Tetrimino()
        {

            InitializeComponent();

            Random r = new Random();

            int i = r.Next(7);

            clear();

            switch (i)
            {
                case 0:
                    tetmino[2, 0] = 1;
                    tetmino[2, 1] = 1;
                    tetmino[2, 2] = 1;
                    tetmino[2, 3] = 1;
                    break;
                case 1:
                    tetmino[2, 1] = 1;
                    tetmino[2, 2] = 1;
                    tetmino[3, 1] = 1;
                    tetmino[3, 2] = 1;
                    break;
                case 2:
                    tetmino[2, 1] = 1;
                    tetmino[2, 2] = 1;
                    tetmino[3, 0] = 1;
                    tetmino[3, 1] = 1;
                    break;
                case 3:
                    tetmino[2, 0] = 1;
                    tetmino[2, 1] = 1;
                    tetmino[3, 1] = 1;
                    tetmino[3, 2] = 1;
                    break;
                case 4:
                    tetmino[2, 0] = 1;
                    tetmino[2, 1] = 1;
                    tetmino[2, 2] = 1;
                    tetmino[3, 2] = 1;
                    break;
                case 5:
                    tetmino[2, 0] = 1;
                    tetmino[2, 1] = 1;
                    tetmino[2, 2] = 1;
                    tetmino[3, 0] = 1;
                    break;
                case 6:
                    tetmino[2, 0] = 1;
                    tetmino[2, 1] = 1;
                    tetmino[2, 2] = 1;
                    tetmino[3, 1] = 1;
                    break;
                default:
                    break;
            }

        }

        /* tetminoの初期化 */
        public void clear()
        {
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    tetmino[i, j] = 0;
                }
            }

        }


        
    }
}
