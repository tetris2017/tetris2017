﻿namespace Tetris
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.Next_lbl = new System.Windows.Forms.Label();
            this.Score_lbl = new System.Windows.Forms.Label();
            this.Score = new System.Windows.Forms.Label();
            this.Level = new System.Windows.Forms.Label();
            this.Level_lbl = new System.Windows.Forms.Label();
            this.Hold = new System.Windows.Forms.Label();
            this.hold1 = new Tetris.Hold();
            this.field = new Tetris.Field();
            this.Next_tetymino = new Tetris.Next_tetymino();
            this.score_panel = new System.Windows.Forms.Panel();
            this.next_panel = new System.Windows.Forms.Panel();
            this.hold_panel = new System.Windows.Forms.Panel();
            this.High_core = new System.Windows.Forms.Label();
            this.High_score_lbl = new System.Windows.Forms.Label();
            this.score_panel.SuspendLayout();
            this.next_panel.SuspendLayout();
            this.hold_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // Next_lbl
            // 
            this.Next_lbl.AutoSize = true;
            this.Next_lbl.BackColor = System.Drawing.Color.Transparent;
            this.Next_lbl.Font = new System.Drawing.Font("Segoe WP", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Next_lbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(58)))), ((int)(((byte)(1)))));
            this.Next_lbl.Location = new System.Drawing.Point(48, 10);
            this.Next_lbl.Margin = new System.Windows.Forms.Padding(0);
            this.Next_lbl.Name = "Next_lbl";
            this.Next_lbl.Size = new System.Drawing.Size(67, 30);
            this.Next_lbl.TabIndex = 4;
            this.Next_lbl.Text = "NEXT";
            // 
            // Score_lbl
            // 
            this.Score_lbl.Font = new System.Drawing.Font("Segoe WP", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Score_lbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(58)))), ((int)(((byte)(1)))));
            this.Score_lbl.Location = new System.Drawing.Point(19, 109);
            this.Score_lbl.Name = "Score_lbl";
            this.Score_lbl.Size = new System.Drawing.Size(101, 21);
            this.Score_lbl.TabIndex = 5;
            this.Score_lbl.Text = "0";
            this.Score_lbl.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Score
            // 
            this.Score.AutoSize = true;
            this.Score.Font = new System.Drawing.Font("Segoe WP", 15.75F, System.Drawing.FontStyle.Bold);
            this.Score.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(58)))), ((int)(((byte)(1)))));
            this.Score.Location = new System.Drawing.Point(36, 79);
            this.Score.Name = "Score";
            this.Score.Size = new System.Drawing.Size(79, 30);
            this.Score.TabIndex = 6;
            this.Score.Text = "SCORE";
            // 
            // Level
            // 
            this.Level.AutoSize = true;
            this.Level.Font = new System.Drawing.Font("Segoe WP", 15.75F, System.Drawing.FontStyle.Bold);
            this.Level.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(58)))), ((int)(((byte)(1)))));
            this.Level.Location = new System.Drawing.Point(49, 143);
            this.Level.Name = "Level";
            this.Level.Size = new System.Drawing.Size(71, 30);
            this.Level.TabIndex = 7;
            this.Level.Text = "LEVEL";
            // 
            // Level_lbl
            // 
            this.Level_lbl.Font = new System.Drawing.Font("Segoe WP", 14.25F, System.Drawing.FontStyle.Bold);
            this.Level_lbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(58)))), ((int)(((byte)(1)))));
            this.Level_lbl.Location = new System.Drawing.Point(19, 173);
            this.Level_lbl.Name = "Level_lbl";
            this.Level_lbl.Size = new System.Drawing.Size(101, 21);
            this.Level_lbl.TabIndex = 10;
            this.Level_lbl.Text = "1";
            this.Level_lbl.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Hold
            // 
            this.Hold.AutoSize = true;
            this.Hold.BackColor = System.Drawing.Color.Transparent;
            this.Hold.Font = new System.Drawing.Font("Segoe WP", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Hold.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(58)))), ((int)(((byte)(1)))));
            this.Hold.Location = new System.Drawing.Point(55, 10);
            this.Hold.Margin = new System.Windows.Forms.Padding(10);
            this.Hold.Name = "Hold";
            this.Hold.Size = new System.Drawing.Size(71, 30);
            this.Hold.TabIndex = 12;
            this.Hold.Text = "HOLD";
            // 
            // hold1
            // 
            this.hold1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.hold1.BackColor = System.Drawing.Color.Transparent;
            this.hold1.Location = new System.Drawing.Point(20, 44);
            this.hold1.Margin = new System.Windows.Forms.Padding(16, 15, 16, 15);
            this.hold1.Name = "hold1";
            this.hold1.Size = new System.Drawing.Size(103, 53);
            this.hold1.TabIndex = 11;
            // 
            // field
            // 
            this.field.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(221)))), ((int)(((byte)(135)))));
            this.field.Location = new System.Drawing.Point(168, 1);
            this.field.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.field.Name = "field";
            this.field.Size = new System.Drawing.Size(295, 507);
            this.field.TabIndex = 0;
            this.field.Next_draw += new System.EventHandler(this.field_Next_draw);
            this.field.End += new System.EventHandler(this.field_End);
            this.field.Score_draw += new System.EventHandler(this.filed_Score_draw);
            this.field.High_score_draw += new System.EventHandler(this.field_High_score_draw);
            this.field.Level_draw += new System.EventHandler(this.field_Level_draw);
            this.field.Hold_draw += new System.EventHandler(this.field_Hold_draw);
            // 
            // Next_tetymino
            // 
            this.Next_tetymino.BackColor = System.Drawing.Color.Transparent;
            this.Next_tetymino.Location = new System.Drawing.Point(17, 44);
            this.Next_tetymino.Margin = new System.Windows.Forms.Padding(20, 15, 16, 15);
            this.Next_tetymino.Name = "Next_tetymino";
            this.Next_tetymino.Size = new System.Drawing.Size(103, 53);
            this.Next_tetymino.TabIndex = 3;
            // 
            // score_panel
            // 
            this.score_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(221)))), ((int)(((byte)(135)))));
            this.score_panel.Controls.Add(this.High_score_lbl);
            this.score_panel.Controls.Add(this.High_core);
            this.score_panel.Controls.Add(this.Level_lbl);
            this.score_panel.Controls.Add(this.Level);
            this.score_panel.Controls.Add(this.Score_lbl);
            this.score_panel.Controls.Add(this.Score);
            this.score_panel.Location = new System.Drawing.Point(479, 207);
            this.score_panel.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.score_panel.Name = "score_panel";
            this.score_panel.Size = new System.Drawing.Size(139, 247);
            this.score_panel.TabIndex = 13;
            this.score_panel.Paint += new System.Windows.Forms.PaintEventHandler(this.score_panel_Paint);
            // 
            // next_panel
            // 
            this.next_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(221)))), ((int)(((byte)(135)))));
            this.next_panel.Controls.Add(this.Next_lbl);
            this.next_panel.Controls.Add(this.Next_tetymino);
            this.next_panel.Location = new System.Drawing.Point(479, 59);
            this.next_panel.Name = "next_panel";
            this.next_panel.Size = new System.Drawing.Size(139, 121);
            this.next_panel.TabIndex = 14;
            this.next_panel.Paint += new System.Windows.Forms.PaintEventHandler(this.next_panel_Paint);
            // 
            // hold_panel
            // 
            this.hold_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(221)))), ((int)(((byte)(135)))));
            this.hold_panel.Controls.Add(this.Hold);
            this.hold_panel.Controls.Add(this.hold1);
            this.hold_panel.Location = new System.Drawing.Point(12, 59);
            this.hold_panel.Name = "hold_panel";
            this.hold_panel.Size = new System.Drawing.Size(139, 121);
            this.hold_panel.TabIndex = 15;
            this.hold_panel.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // High_core
            // 
            this.High_core.AutoSize = true;
            this.High_core.BackColor = System.Drawing.Color.Transparent;
            this.High_core.Font = new System.Drawing.Font("Segoe WP", 15.75F, System.Drawing.FontStyle.Bold);
            this.High_core.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(58)))), ((int)(((byte)(1)))));
            this.High_core.Location = new System.Drawing.Point(62, 27);
            this.High_core.Name = "High_core";
            this.High_core.Size = new System.Drawing.Size(53, 30);
            this.High_core.TabIndex = 16;
            this.High_core.Text = "TOP";
            // 
            // High_score_lbl
            // 
            this.High_score_lbl.BackColor = System.Drawing.Color.Transparent;
            this.High_score_lbl.Font = new System.Drawing.Font("Segoe WP", 14.25F, System.Drawing.FontStyle.Bold);
            this.High_score_lbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(58)))), ((int)(((byte)(1)))));
            this.High_score_lbl.Location = new System.Drawing.Point(19, 57);
            this.High_score_lbl.Name = "High_score_lbl";
            this.High_score_lbl.Size = new System.Drawing.Size(101, 21);
            this.High_score_lbl.TabIndex = 17;
            this.High_score_lbl.Text = "0";
            this.High_score_lbl.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(58)))), ((int)(((byte)(1)))));
            this.ClientSize = new System.Drawing.Size(630, 510);
            this.Controls.Add(this.hold_panel);
            this.Controls.Add(this.next_panel);
            this.Controls.Add(this.score_panel);
            this.Controls.Add(this.field);
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Activated += new System.EventHandler(this.Form1_Enter);
            this.Enter += new System.EventHandler(this.Form1_Enter);
            this.score_panel.ResumeLayout(false);
            this.score_panel.PerformLayout();
            this.next_panel.ResumeLayout(false);
            this.next_panel.PerformLayout();
            this.hold_panel.ResumeLayout(false);
            this.hold_panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public Field field;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label Next_lbl;
        public Next_tetymino Next_tetymino;
        private System.Windows.Forms.Label Score_lbl;
        private System.Windows.Forms.Label Score;
        private System.Windows.Forms.Label Level;
        private System.Windows.Forms.Label Level_lbl;
        private Hold hold1;
        private System.Windows.Forms.Label Hold;
        private System.Windows.Forms.Panel score_panel;
        private System.Windows.Forms.Panel next_panel;
        private System.Windows.Forms.Panel hold_panel;
        private System.Windows.Forms.Label High_core;
        private System.Windows.Forms.Label High_score_lbl;
    }
}

