﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tetris
{
    public partial class Hold : UserControl
    {
        private static List<Point> hold_ls = new List<Point>();

        Color color3 = Color.FromArgb(115, 128, 5);
        Color color4 = Color.FromArgb(43, 58, 1);          //黒


        public Hold()
        {
            InitializeComponent();
        }


        public void Hold_drawing(int[,] hold)
        {

            hold_ls.Clear();

            int i, j;

            for (i = 2; i < 4; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    if ((hold[i, j]) == 1)
                    {
                        Point p = new Point();

                        p.Y = (i - 2) * 25 + 1;
                        p.X = j * 25 + 1;

                        hold_ls.Add(p);
                    }
                }
            }

            this.Invalidate();
        }


        private void Hold_Paint(object sender, PaintEventArgs e)
        {
            //グラフィックオブジェクトの取得
            Graphics g = e.Graphics;

            /* テトリミノ */
            Pen dp = new Pen(color4, 3);
            SolidBrush b = new SolidBrush(color3);

            foreach (Point p in hold_ls)
            {
                int x = p.X;
                int y = p.Y;
                g.FillRectangle(b, x, y, 25, 25);
                g.DrawRectangle(dp, x, y, 25, 25);
            }
            Field_focus?.Invoke(this, EventArgs.Empty);

        }

        public event EventHandler Field_focus;

    }
}
