﻿namespace Tetris
{
    partial class Field
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.gameover_lbl = new System.Windows.Forms.Label();
            this.start_lbl = new System.Windows.Forms.Label();
            this.easy = new System.Windows.Forms.Label();
            this.hard = new System.Windows.Forms.Label();
            this.nomal = new System.Windows.Forms.Label();
            this.gameover_lbl2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 20;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // gameover_lbl
            // 
            this.gameover_lbl.AutoSize = true;
            this.gameover_lbl.BackColor = System.Drawing.Color.Transparent;
            this.gameover_lbl.Font = new System.Drawing.Font("Segoe WP", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gameover_lbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(58)))), ((int)(((byte)(1)))));
            this.gameover_lbl.Location = new System.Drawing.Point(24, 136);
            this.gameover_lbl.Name = "gameover_lbl";
            this.gameover_lbl.Size = new System.Drawing.Size(159, 37);
            this.gameover_lbl.TabIndex = 0;
            this.gameover_lbl.Text = "Game Over";
            this.gameover_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // start_lbl
            // 
            this.start_lbl.AutoSize = true;
            this.start_lbl.BackColor = System.Drawing.Color.Transparent;
            this.start_lbl.Font = new System.Drawing.Font("Segoe WP", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.start_lbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(58)))), ((int)(((byte)(1)))));
            this.start_lbl.Location = new System.Drawing.Point(55, 136);
            this.start_lbl.Name = "start_lbl";
            this.start_lbl.Size = new System.Drawing.Size(172, 37);
            this.start_lbl.TabIndex = 1;
            this.start_lbl.Text = "Start - Enter";
            // 
            // easy
            // 
            this.easy.BackColor = System.Drawing.Color.Transparent;
            this.easy.Font = new System.Drawing.Font("Segoe WP Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.easy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(58)))), ((int)(((byte)(1)))));
            this.easy.Location = new System.Drawing.Point(67, 258);
            this.easy.Margin = new System.Windows.Forms.Padding(5);
            this.easy.Name = "easy";
            this.easy.Padding = new System.Windows.Forms.Padding(3);
            this.easy.Size = new System.Drawing.Size(120, 25);
            this.easy.TabIndex = 2;
            this.easy.Text = "やさしい";
            this.easy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // hard
            // 
            this.hard.BackColor = System.Drawing.Color.Transparent;
            this.hard.Font = new System.Drawing.Font("Segoe WP", 12F, System.Drawing.FontStyle.Bold);
            this.hard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(58)))), ((int)(((byte)(1)))));
            this.hard.Location = new System.Drawing.Point(67, 328);
            this.hard.Margin = new System.Windows.Forms.Padding(5);
            this.hard.Name = "hard";
            this.hard.Padding = new System.Windows.Forms.Padding(3);
            this.hard.Size = new System.Drawing.Size(120, 25);
            this.hard.TabIndex = 3;
            this.hard.Text = "むずかしい";
            this.hard.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // nomal
            // 
            this.nomal.BackColor = System.Drawing.Color.Transparent;
            this.nomal.Font = new System.Drawing.Font("Segoe WP", 12F, System.Drawing.FontStyle.Bold);
            this.nomal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(58)))), ((int)(((byte)(1)))));
            this.nomal.Location = new System.Drawing.Point(67, 293);
            this.nomal.Margin = new System.Windows.Forms.Padding(5);
            this.nomal.Name = "nomal";
            this.nomal.Padding = new System.Windows.Forms.Padding(3);
            this.nomal.Size = new System.Drawing.Size(120, 25);
            this.nomal.TabIndex = 4;
            this.nomal.Text = "ふつう";
            this.nomal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gameover_lbl2
            // 
            this.gameover_lbl2.AutoSize = true;
            this.gameover_lbl2.BackColor = System.Drawing.Color.Transparent;
            this.gameover_lbl2.Font = new System.Drawing.Font("Segoe WP", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gameover_lbl2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(58)))), ((int)(((byte)(1)))));
            this.gameover_lbl2.Location = new System.Drawing.Point(57, 258);
            this.gameover_lbl2.Name = "gameover_lbl2";
            this.gameover_lbl2.Size = new System.Drawing.Size(153, 60);
            this.gameover_lbl2.TabIndex = 0;
            this.gameover_lbl2.Text = "Continue - Esc\r\nEnd - F1";
            this.gameover_lbl2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Field
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gameover_lbl2);
            this.Controls.Add(this.nomal);
            this.Controls.Add(this.hard);
            this.Controls.Add(this.easy);
            this.Controls.Add(this.start_lbl);
            this.Controls.Add(this.gameover_lbl);
            this.Name = "Field";
            this.Size = new System.Drawing.Size(274, 524);
            this.Load += new System.EventHandler(this.Field_Load);
            this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.Field_PreviewKeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label gameover_lbl;
        private System.Windows.Forms.Label start_lbl;
        private System.Windows.Forms.Label easy;
        private System.Windows.Forms.Label hard;
        private System.Windows.Forms.Label nomal;
        private System.Windows.Forms.Label gameover_lbl2;
    }
}
