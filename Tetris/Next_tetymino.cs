﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tetris
{
    public partial class Next_tetymino : UserControl
    {

        private static List<Point> next_tetr_ls = new List<Point>();

        Color color3 = Color.FromArgb(115, 128, 5);           //濃い黒
        Color color4 = Color.FromArgb(43, 58, 1);          //黒


        public Next_tetymino()
        {
            InitializeComponent();
        }

      
        public void Next_drawing(int[,] next)
        {
            next_tetr_ls.Clear();

            int i, j;

            for (i = 2; i < 4; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    if ((next[i, j]) == 1)
                    {
                        Point p = new Point();

                        p.Y = (i - 2) * 25 + 1;
                        p.X = j * 25 + 1;

                        next_tetr_ls.Add(p);
                    }
                }
            }
            this.Invalidate();
        }


        private void Next_tetymino_Paint(object sender, PaintEventArgs e)
        {
            //グラフィックオブジェクトの取得
            Graphics g = e.Graphics;

            /* テトリミノ */
            Pen dp = new Pen(color4, 3);
            SolidBrush b = new SolidBrush(color3);

            foreach (Point p in next_tetr_ls)
            {
                int x = p.X;
                int y = p.Y;
                g.FillRectangle(b, x, y, 25, 25);
                g.DrawRectangle(dp, x, y, 25, 25);
            }
            
            Field_focus?.Invoke(this, EventArgs.Empty);

        }

        public event EventHandler Field_focus;

    }
}
